﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using LiveCharts;
using LiveCharts.Wpf;

namespace statCPU1
{
    public partial class Form1 : Form
    {
        List<double> procent = new List<double>();
        PerformanceCounter _cpuTotal = new PerformanceCounter() 
        { 
            InstanceLifetime = PerformanceCounterInstanceLifetime.Global,
            CategoryName = "Processor",
            InstanceName = "_Total",
            CounterName = "% Processor Time",
            MachineName = ".",
            ReadOnly = true
        };

        double CPUCounter;

        public Form1()
        {
            InitializeComponent();

            cartesianChart1.AxisX.Add(new Axis{});

            cartesianChart1.AxisY.Add(new Axis
            {
                Title = "Count"
            });

            var series = new LineSeries
            {
                Title = "",
                Values = new ChartValues<double>(procent),
                DataLabels = true,
            };

            cartesianChart1.Series.Add(series);
        }
        private void CpuLoader()
        {
            while (true) 
            {
                CPUCounter = Math.Round(_cpuTotal.NextValue(), 2);
                Rezult();
                Thread.Sleep(1000);
            }
        }
     /// <summary>
     /// Rezult
     /// </summary>
        void Rezult()
        {            
            procent.Add(CPUCounter);

            if (procent.Count >= 10)
                procent.RemoveAt(0);

            Grafic();
            if (richTextBox1.InvokeRequired)
            {
                richTextBox1.Invoke((MethodInvoker)(() =>
                {
                    richTextBox1.AppendText(CPUCounter.ToString());
                    richTextBox1.Text = richTextBox1.Text + Environment.NewLine;
                }));
            }
            else
            {
                richTextBox1.AppendText(CPUCounter.ToString());
                richTextBox1.Text = richTextBox1.Text + Environment.NewLine;
            }
        }
        void Grafic()
        {
            if (richTextBox1.InvokeRequired)
            {
                richTextBox1.Invoke((MethodInvoker)(() =>
                {
                    cartesianChart1.Series[0].Values.Add(procent.Last());                    
                }));
            }
            else
            {
                cartesianChart1.Series[0].Values.Add(procent.Last());
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();

            Thread start = new Thread(CpuLoader);
            start.Start();
        }        
    }
}





    